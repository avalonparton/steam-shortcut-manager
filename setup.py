from setuptools import setup

with open("README.md", "r") as fh:
    long_description = fh.read()

setup(name='steammanager',
      version='0.6',
      author='Avalon Parton',
      author_email='avalonlee@gmail.com',
      description='Steam Shortcut Manager',
      long_description=long_description,
      long_description_content_type="text/markdown",
      license='MIT',
      url='https://gitlab.com/avalonparton/steam-shortcut-manager',
      packages=['steammanager'],
      install_requires=['steamclient'],
      zip_safe=False)
